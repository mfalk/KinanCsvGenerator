<!DOCTYPE html>
<?php
function generateUsername($length = 15) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function generatePassword($length = 16) {
	
	$randomString = '';
	
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    for ($i = 0; $i < $length/4; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
	
	$characters = 'abcdefghijklmnopqrstuvwxyz';
    $charactersLength = strlen($characters);
    for ($i = 0; $i < $length/4; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
	
	$characters = '!@^?#&$+()';
    $charactersLength = strlen($characters);
    for ($i = 0; $i < $length/4; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
	
	$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    for ($i = 0; $i < $length/4; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
	
    return $randomString;
}

function generateDate() {
	$int = mt_rand(412055681,812055681);
	$randomString = date("Y-m-d",$int);

	return $randomString;
}

$generateSubmit = $_POST['generateSubmit'];
?>

<html>
	<head>
		<title>Generator for KinanCity-core</title>
	</head>
	<body>
	<?php
if(isset($generateSubmit)){
	$domain = $_POST['domain'];
	$countryCode = $_POST['countryCode'];
	
	if(empty($countryCode)){
		$countryCode = "US";
	}
	
	echo "#username;password;email;dob;country<br>";
	for ($i = 0; $i < $_POST['numberOfAccounts']; $i++) {
		$randomUsername = generateUsername();
		$randomPassword = generatePassword();
		$randomDate = generateDate();
		
		echo "{$randomUsername};{$randomPassword};{$randomUsername}@{$domain};{$randomDate};{$countryCode}<br>";
	}
	
} else {
?>
	Save the output as accounts.csv in the same directory as KinanCore.<br>
	Run the commmand below to generate the accounts:<br>
	<b>java -jar KinanCity-core.jar -a accounts.csv -t NumberOfThreads</b>
	
	<form action="" method="POST">
		<ul style="list-style-type: none">
			<li>Domain Name <input type="text" name="domain" placeholder="MyDomain.com" required><li>
			<li>Country Code <input type="text" name="countryCode" placeholder="Country Code, 2 letters(US)" maxlength="2"></li>
			<li>Accounts to generate <input type="number" name="numberOfAccounts" min="1" max="50000" required></li>
			<li><input type='submit' name='generateSubmit'></li>
		</ul>
	</form>
<?php
}
?>
	</body>
</html>